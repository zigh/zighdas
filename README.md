## **Zigh DAS - Cybersecurity for everyone**




**Zigh DAS** is an open-source cybersecurity project using Powershell Core cmdlets available to anyone.


**DAS protocol** is a concept of cybersecurity that turns safety measures into counter-attacks against hackers.


**Powershell** is an open-source and cross-platform framework by Microsoft that hackers love.


**Remember** that we are NOT responsible for the problems Powershell gives you. Microsoft is.




## **Scenario One:** you are a simple PC user but don't run Powershell (PS), never(!).


**Windows OS** - Best practices: disable Powershell script executions on Local Group Policy Editor, disable Powershell 1, 2, 3 wtf else. Don't install newest PS versions nor PS core. Google it to find the best setups. AND, don't permit any kind of Windows system remote access nor telemetry. PS is NOT user-friendly in terms of privacy and security. Worsen, it's also a free way for hackers. Until further notice, Microsoft hasn't corrected it yet and it's so difficult to get 100% safety even if you know a little bit of coding. Windows OS has many hidden doors...


**Other OS** - are you crazy? Wanna buy $MSFT protection? Today (covid pandemic days), ransomware attacks can encrypt Linux servers. Why? Don't be a digital fool. Don't install it anyway or even permit PS running inside your machine. Block remote access and telemetry. Set some configurations you can find on web. Powershell is also a dirty tool for hackering explotations and pentests. Forget about it. One day, Microsoft can create, for example, a cross-platform checking tool by passwords any time a third-party system wants freely install a program via PS without adm permissions. It doesn't exist yet. Again, don't permit remote access nor telemetry.




## **Scenario Two:** hell, you are a simple PC user and run PS sometimes...


First of all, you should know and set the right Execution Policy and/or Group Policy so that you can prevent hackering explotations. Execution policies can be Restricted, RemoteSigned, AllSigned and Unrestricted. Powershell has a default configuration and you must learn how to set them [properly](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.security/set-executionpolicy?view=powershell-7#:~:text=The%20effective%20execution%20policy%20is%20determined%20by%20the,scope%20that%20affects%20all%20users%20of%20the%20computer.). It's boring but also a crucial task for safety. Google it.


**USB drive attacks**


You must lock/block USB input possibilities when you're away from your machine... To disable USB port, open PS as adm and run:


`Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Services\USBSTOR\" -Name "start" -Value 4`


Hackers can stick USB devices yet but your computer won't read them at all. To enable USB port again, open PS as adm and run:


`Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Services\USBSTOR\" -Name "start" -Value 3`


It's easy and fast. Do it whenever and how many times you want and remember scripts always change. So, Microsoft devs can create another service to sell us and those cmdlets won't work no more. But you should ask me if hackers could bypass your safety settings and install malwares using PS even so. Well, they can. That's why we need extra cmdlets to make our privacy and security better. Open PS as adm and run `Disable-PSRemoting -Force` to prevent remote access to your machine by payload attacks. Also run as adm, `Stop-Service WinRM -Force` to stop Windows remote service (it's default and you should stop it too). One more thing, `Set-NetFirewallRule -DisplayName 'Windows Remote Management (HTTP-In)' -Enabled False` as a rule for Windows Firewall halts possible hackering requests via web. Anytime you want, change them running reversing cmdlets you can find on PS [docs](https://docs.microsoft.com/en-us/powershell/scripting/developer/cmdlet/cmdlet-overview?view=powershell-7).


If you prefer, run PS as adm all together `Disable-PSRemoting -Force; Stop-Service WinRM -Force; Set-NetFirewallRule -DisplayName 'Windows Remote Management (HTTP-In)' -Enabled False` to get the same result. Cybercriminals always change their modus operandi and USB attacks mean physical human access is an efficient vector to explotations. Be safe. Something I've never tried and I don't know if it's ok to protect my machine is:


`$format_usb = Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Services\USBSTOR\" -Name "start" -Value 3`

`$format_usb`

`while ($format_usb)` `{Format-Volume -DriveLetter 'letter of USB drive' -FileSystem NTFS -Full -Force; Format-Volume -DriveLetter 'letter of USB drive' -FileSystem FAT32 -Full -Force; Format-Volume -DriveLetter 'letter of USB drive' -FileSystem exFAT -Full -Force}`

where 'letter of USB drive' can be D, E, F... it depends on your machine 😈

Why not 'fuck the duck'?

`$fuck_the_duck = Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Services\USBSTOR\" -Name "start" -Value 4`

`$fuck_the_duck`

`while ( not($fuck_the_duck)) {
    Format-Volume -DriveLetter 'letter of USB drive' -FileSystem NTFS -Full -Force; Format-Volume -DriveLetter 'letter of USB drive' -FileSystem FAT32 -Full -Force; Format-Volume -DriveLetter 'letter of USB drive' -FileSystem exFAT -Full -Force
}`




I'm a Forensics, OSINT researcher and hope to show you system vulnerabilities here.


ZighDAS - open-source cybersecurity project
    Copyright (C) 2020  ZighDAS <Sandro Silva>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

